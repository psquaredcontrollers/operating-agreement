# Cooperative Operating Agreement for P Squared Controllers, LLC #

The operating agreement for P Squared Controllers is based on the template provided the [Democracy at Work Institute][1] at the [US Federation of Worker Cooperatives][2] at their website.  The template and others are provided [here][3].

**See the [`Processed/`](https://bitbucket.org/psquaredcontrollers/operating-agreement/src/5a71c0b47a57933149d038e316e5ef69956f2542/Processed/?at=master) folder for the latest PDF version.**

It is our hope that sharing this may be helpful and encourage others trying to start a cooperatively run company.

## Some Differences from the Template ##

* All references to the "Permanent Capital Member" have been removed.  

* Added is a "Permanent Member" status, which was modeled after a manufacturing facility in Ohio, whose employees bought it from the owner and transformed it into a worker cooperative.  The founding members will be considered "Permanent Members" until their collective contribution (measured by percentage share) is no longer as significant as it once was.  This is to discourage some like an unproductive tenure.

* Voting strength for "Provisional Members" is 1/3, rather than 1/2, of an "Active Member".  This is to help protect the priorities of the initial members (who we can count, currently, with one hand).

## Processed Files and ReST Rendering Engines ##

ReStructeredText (ReST) was used for the Operating Agreement because they can handle enumerated lists.  That is, lists with numbers (*1. 2. 3.*), letters (*(a) (b) (c)*), and even roman numerals (*(i) (ii) (iii)*).  The files were processed into PDF format using [Pandoc][4].  The command used was

    pandoc -s -t latex -o Processed/operating_agreement.pdf operating_agreement.rs

Rendering engines for markdown or ReST are not standardized, and they don't always come out how you might expect them.  For example, the lists generated do not actually use the numbered bullets you specify, but create the lists then use styling to add labels.  The implication is that if you try to (or mistakenly) skip a number, the rendering engine will change the numbers and potential break any references you have.  For example *"Section 2.5(c)(i)"* could be different in your markdown/ReST and your rendered doc.  This includes the preview created on sites like Github or here on Bitbucket.  So **be careful!**

## Disclaimer ##

This repository does not provide legal advice and does not create an attorney-client relationship. If you need legal advice, please contact an attorney directly.

[1]: http://institute.coop/
[2]: https://usworker.coop/home/
[3]: http://institute.coop/bylaw-and-operating-agreement-templates
[4]: http://pandoc.org/