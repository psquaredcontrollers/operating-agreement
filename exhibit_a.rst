Exhibit A
#########

Exhibit A for the Operating Agreement for P Squared Controllers LLC

:Revision: Initial Revision
:Date: 2017-01-06

Allocation of Net Income, Etc.
******************************

1.  **“Net Income”** and **“Net Loss”** shall mean, for each Fiscal Year or other period, an amount equal to the Company’s taxable income or loss for the year or period, determined in accordance with Code section 703(a) (for this purpose, all items of income, gain, loss, or deduction required to be stated separately pursuant to Code section 703(a)(1) shall be included in taxable income or loss), with the following adjustments:

    (a) Any income that is exempt from federal income tax shall be added to the Company’s gross income;

    (b) Any expenditures of the Company described in Code section 705(a)(2)(B) (or treated by Treasury Regulations as if described in that section) shall be treated as a deduction of the Company;

    (c) Gain or loss from any disposition of Company property shall be computed with reference to the book value of the property if its book value differs from its adjusted tax basis;

    (d) If the book value of any Company property differs from its adjusted tax basis, depreciation, amortization, or other cost recovery deductions with respect to the property shall be computed with reference to the book value;

    (e) Any items which are specially allocated pursuant to Section 5 shall be taken into account prior to computing, and shall be excluded from the computation of, Net Income or Net Loss for purposes of this Section 1; and

    (f) Any other adjustments which the tax matters partner determines are necessary to comply with Treasury Regulations section 1.704-1(b) shall be made.

2.  **Capital Accounts:** A separate capital account (a **“Capital Account”**) shall be maintained for each Member for the full term of this Agreement in accordance with the capital accounting rules of section 1.704(b)(2)(iv) of the Treasury Regulations.

3.  **Allocation of Net Income:** After giving effect to the special allocations set forth in Section 5, Net Income for any taxable year shall be allocated among the Members as follows:

    (a) *[Intentionally left blank]*

    (b) Net Income shall be allocated to the Provisional, Active and Inactive Members in proportion to, but not in excess of, the amounts distributed to each during such taxable year;

4.  **Allocation of Net Loss:** After giving effect to the special allocations set forth in Section 5, Net Loss for any taxable year shall be allocated among the Members as follows:

    (a) *[Intentionally left blank]*

    (b) Net Loss shall be allocated to the Provisional, Active and Inactive Members in proportion to their Capital Accounts, except that in no event shall Net Loss be allocated to any such Member if such allocation would reduce such Member’s Capital Account balance to less than a deficit amount equal to the amount such Member is deemed obligated to restore to its Capital Account pursuant to the next to last sentences of Treasury Regulations sections 1.704-2(g)(1) and (i)(5).

5.  **Special Allocations:** If Company property is reflected in the Capital Accounts and on the books of the Company at a book value that differs from the adjusted tax basis of the property, then depreciation, depletion, amortization, and gain or loss, as computed for tax purposes, with respect to the property, shall be determined and allocated among the Members, solely for tax purposes, so as to take account of the variation between the adjusted tax basis and the book value of the property in any reasonable method determined by the Active and Provisional Members and permitted under section 704(c) of the Code and applicable Treasury Regulations.

6.  **Other Allocation and Distribution Rules**

    (a) For purposes of determining Net Income or Net Loss, or any other items allocable to any period, Net Income, Net Loss and any such other items shall be determined on a daily, monthly, or other basis, as determined by the tax matters partner using any permissible method under Code section 706 and Treasury Regulations thereunder.

    (b) Except as otherwise provided in this Agreement, all items of Company income, gain, loss, deduction and any other allocations not otherwise provided for shall be divided among the Members in the same proportions that they share Net Income or Net Loss, as the case may be.

    (c) Any amounts withheld by the Company pursuant to the Code or any provision of any state or local law with respect to any allocation or distribution to a Member shall be treated as a distribution to the Member for all purposes under this Agreement.

7.  **Valuation of Company Assets:** The book values of all Company assets shall be adjusted to equal their respective fair market values (taking Code section 7701(g) into account), as reasonably determined by the Active and Provisional Members, upon the occurrence of any of the following events: (i) a contribution of money or property (other than a de minimis amount) to the Company by a new or existing Member as consideration for a membership interest; (ii) a distribution of money or property (other than a de minimis amount) by the Company to a Member as consideration for a membership interest; and (iii) the liquidation of the Company within the meaning of Treasury Regulations section 1.704-1(b)(2)(ii)(g); provided, however, that the adjustments pursuant to clauses (i) and (ii) of this sentence shall be made only if the Active and Provisional Members reasonably determine that such adjustments are necessary or appropriate to reflect the relative economic interest of the Members. Any such adjustments shall be reflected by corresponding adjustments to the Capital Accounts which reflect the manner in which the unrealized income, gain, loss, or deduction inherent in such property (that has not been reflected in the Capital Accounts previously) would be allocated among the Members if there were a taxable disposition of such assets for such fair market values.
