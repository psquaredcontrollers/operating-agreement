Initial Signature Page
######################

Initial Signature Page to Operating Agreement for P Squared Controllers LLC

:Date: 2017-01-06
:Revision: Initial Revision

Initial Members:
----------------

| IN WITNESS WHEREOF, the undersigned have signed this Agreement.
|
|
|
| Signature: ______________________ Date: __________
|
| Printed Name: __________________________________
|
|
|
| Signature: ______________________ Date: __________
|
| Printed Name: __________________________________
|
|
|
| Signature: ______________________ Date: __________
|
| Printed Name: __________________________________
