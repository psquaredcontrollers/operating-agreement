Operating Agreement
####################

This Operating Agreement of P Squared Controllers, LLC, an Ohio limited liability company (the “Company”), is entered into effective as of 06 January, 2017, among the individuals on the initial signature page hereof, and any other individuals who sign an additional signature page hereof as members admitted in accordance with this Agreement (each a “Member” and collectively, the “Members”).

:Date: 2017-01-06
:Revision: Initial Revision

Section 1:  Formation
*********************

1. **Formation of Company**

    The Company has been organized as an Ohio limited liability company by the filing of the Articles of Organization pursuant to the Ohio Revised Code, Chapter 1705 (“Act”) with the Ohio Secretary of State.

2. **Name**

    The name of the Company is “P Squared Controllers” and all Company business shall be conducted under that name or such other names that comply with applicable law as the Active Members may select from time to time.

3. **Purpose and Scope**

    Subject to the provisions of this Agreement and the Articles, the general purpose of the Company is to generate sustainable profits for its Active Members through an entity that operates based on democratic principles. Its initial scope of operations will be design, manufacture, and sale of electronics devices and all activities related thereto, but the Active Members may authorize a different or additional scope of operations.

4. **Term**

    The Company shall commence on the date the Articles are filed and shall continue until dissolved pursuant to the terms of this Agreement or the Act.

5. **Office; Agent**

    The office of the Company required by the Act to be maintained in the State of Ohio shall be located at 5859 Highland Road, Highland Heights, OH 44143, or such other office (which need not be a place of business of the Company) as the Active and Provisional Members may designate from time to time in the manner provided by law. The Company may have such other offices as the Active and Provisional Members may designate from time to time. The Active and Provisional Members may designate and change the Company’s agent for service of process from time to time.

Section 2:  Members, Management
*******************************

1.  **Types of Members**

    The Members shall consist of Provisional, Active, Protected, and Inactive Members. References made in this Agreement to “Member” or “Members” without distinguishing the type of Member shall mean all Members regardless of type.

    (a) **"Provisional Members"** are Members who, at the time of determination, (1) are serving a trial period as new Members in accordance with Section 2.2(b) or (2) existing Members returned to provisional status in accordance with Section 2.2(d). Except as otherwise expressly indicated in this Agreement, each Provisional Member shall have one-third of one vote.

    (b) **“Active Members”** are Members who, at the time of determination, (1) have been admitted as an Active Member in accordance with this Agreement, (2) regularly render services in furtherance of the Company’s business, (3) are (i) an initial Member admitted pursuant to Section 2.2(a) or (ii) successfully completed one or more terms as a Provisional Member and/or Provisional Employee and (4) have not resigned or been removed as an Active Member. Each Active Member shall have one vote.

    (c) **"Inactive Members"** are Members who, at the time of determination, (1) have resigned from Active or Provisional Member status or have been removed from Active or Provisional Member status and (2) are still awaiting payment of distributions from their ICA. Except as otherwise expressly indicated in this Agreement, Inactive Members do not have voting rights. Inactive Members may not undertake any kind of activity on behalf of the Company and automatically cease to be Members (a) upon the Company’s full payment of any distributions due to an Inactive Member from such Inactive Member’s ICA or (b) as may otherwise be agreed upon between the Company and such Inactive Member.

    (d) *[Intentionally left blank]*

    (e) **“Protected Members”** are Active Members who, at the time of determination, (1) have been admitted as a Protected Member in accordance to this Agreement, and (2) have not resigned or been demoted under Section 2.2(h).

2.  **Initial Members; Admission of New Members; Promotion to Active Member; Automatic Removal**

    (a) **Initial Members:**  The Initial Members of the Company are the individuals initially executing this Agreement, each of whom is admitted to the Company as an Active Member, as indicated on the signature page hereof, effective upon the date of this Agreement. All initial Members are considered Protected Members, subject to Section 2.2(h).

    (b) **Admission of New Provisional Members:** Additional Members may be admitted as Provisional Members, provided the following process shall be followed in the admission of any new Member: (1) First, Active and Provisional Members shall have voted whether to admit a candidate for membership as a Provisional Member; and (2) Second, the candidate for membership shall have executed (i) a counterpart of this Agreement, agreeing to be bound hereby and (ii) appropriate acknowledgements that Members are not employees and, therefore, not entitled to benefits employees may be entitled to, including, without limitation, workers’ compensation.

    (c) **Promotion of Provisional Members to Active Members:** Provisional Members admitted under Section 2.2(b) may be promoted to Active Member status by a vote of the Active and Provisional Members. To qualify for such promotion or admission, a Provisional Member shall comply with the required initial capital contribution as and when required by Section 6 of this Agreement and shall have rendered services to the Company as a Provisional Member for no less than six (6) months. In addition, (i) a Provisional Member shall be promoted only upon the vote of the Active and Provisional Members, other than the Member to be promoted. Such vote to promote or admit shall be held within seven (7) months after a Provisional Member has been admitted as a Provisional Member, provided that the Active and Provisional Members may vote to extend such Provisional Member’s Provisional Member status for a term up to an additional seven (7) months.

    (d) **Demotion to Provisional Member; Return to Active Member Status:** An Active Member may be demoted to Provisional Member status, with or without cause, by a vote of the Active and Provisional Members other than the Member in question or pursuant to Section 2.7(b)(3). Members who are Provisional Members by virtue of this Section 2.2(d), which group includes formerly Active Members reinstated as Provisional Members under Section 2.7(b)(3), may be reinstated as Active Members only if, within seven (7) months of becoming a Provisional Member under this Section 2.2(d), the Active and Provisional Members, other than the Member in question, vote to return such Member to Active Member status.

    (e) **Automatic Removal of Provisional Members:** A Provisional Member (i) admitted under Section 2.2(b) who fails to be promoted to Active Member status pursuant to Section 2.2(c) or (ii) who became a Provisional Member under Section 2.2(d) and has not been reinstated within seven (7) months as required by Section 2.2(d), shall be removed automatically as a Provisional Member in accordance with Section 2.7 of this Agreement.

    (f) **Automatic Termination of Provisional Employees** A Provisional Employee who fails to be admitted as an Active Member pursuant to Section 2.2(c) shall be terminated and shall no longer provide services to the Company.

    (g) **Accelerated Promotion of Provisional Members to Active Members:** Notwithstanding Section 2.2(c) and Section 2.1(b)(3)(ii), Provisional Members admitted under Section 2.2(b) may be promoted to Active Member status by a unanimous vote of the Active and Provisional Members. To qualify for such promotion or admission, a Provisional Member shall comply with the required initial capital contribution as and when required by Section 6 of this Agreement.  In addition, (i) a Provisional Member shall be promoted only upon the vote of the Active and Provisional Members, other than the Member to be promoted. Such vote to promote or admit may be held at any time as the Active and Provisional Members may designate.

    (h) **Priveleges and Limitations of Protected Membership:** Notwithstanding Section 2.2(d), Section 2.5(b), and Section 2.7(a)(2), Protected Members shall continue to be considered Active Members so long as the current Percentage Share of the Protected Members, in the aggregate, is more than twenty percent (20%) of the current profits of the Company.

    (i) **Promotion to Protected Member:** Additional Active members may be promoted to Protected Member by a unanimous vote of the Active and Provisional Members (other than Member to be promoted).  Such vote to promote or admit may be held at any time as the Active and Provisional Members may designate.

3.  **Management by Active and Provisional Members**

    The business and affairs of the Company shall be managed by the Active and Provisional Members, who shall have full and complete authority, power, and discretion to manage and control the business, affairs, and properties of the Company, to make all decisions regarding those matters and to perform any and all other acts or activities customary or incident to the management of the Company’s business. Except as otherwise expressly provided in this Agreement, action by the Active and Provisional Members shall be taken by the vote of a majority of the total votes of all Active and Provisional Members, with each having such voting rights as are provided under Section 2.1. The Active and Provisional Members may, from time to time, delegate specified administrative functions and/or authority to act on behalf of and bind the Company in administrative matters to one or more Active or Provisional Members (including without limitation committees of Active or Provisional Members) or other Persons, but such delegated functions and/or authority shall always remain subject to the oversight of the Active and Provisional Members and may be revoked by a vote of the Active and Provisional Members in their sole discretion at any time.

4.  **Binding Action**

    Neither any Provisional or Inactive Member shall have the power or shall take any action which binds or purports to bind the Company to any obligation. Furthermore, no Active Member shall have the power or shall take any action which binds or purports to bind the Company to any obligation except those obligations which the Active and Provisional Members have expressly authorized such Active Member to incur.

5.  **Voluntary Withdrawal; Mandatory Withdrawal**

    Withdrawal by an Active or Provisional Member means that such Member becomes an Inactive Member.

    (a) **Voluntary Withdrawal:** Active and Provisional Members may withdraw as Active or Provisional Members, respectively, at any time, with or without cause, by giving written notice to the Company. Withdrawal by written notice is effective immediately upon receipt by the Company of such written notice.

    (b) **Mandatory Withdrawal:**Active or Provisional Members who have not rendered services in furtherance of the Company’s business for a period determined by the Company from time to time shall be deemed to have withdrawn effective on the last day of such maximum period of inactivity.

.6  **Deceased or Incompetent Member**

    If an Active or Provisional Member dies or is adjudged by a court of competent jurisdiction to be incompetent to manage his or her person or property, such Member will thereupon become an Inactive Member. The Inactive Member’s executor, administrator, guardian, conservator or other legal representative shall be entitled to collect any funds distributable on account of such death or incompetence pursuant to the terms of this Agreement.

.7  **Removal of Member**

    Removal of an Active or Provisional Member means that such Member becomes an Inactive Member and all such Member’s activities on behalf of the Company shall be suspended.

    (a) An Active or Provisional Member shall be removed as a Member if:

        (1) a provision of this Agreement calls for removal of such Member; or
        (2) the Active and Provisional Members other than the Member in question, with or without cause, vote to remove the Member in question.

    (b) Removal of a Member by vote under Section 2.7(a)(2) shall take effect immediately upon the vote, but shall remain effective only if all requirements of this Section 2.7(b) are subsequently fulfilled.

        (1) Notice of a vote to remove under Section 2.7(a)(2) must be mailed or given to the removed Member no later than five (5) days after such vote and must inform the removed Member that he or she has thirty (30) days from the vote to remove to make a written request for a review of the vote to remove. (2) If, and only if, the removed Member makes a written requests for a review of the vote to remove within thirty (30) days of such vote, then a meeting of Active and Provisional Members shall be convened solely for the purpose of reviewing the removal. At such review meeting, the removed Member shall be afforded a reasonable opportunity to address the Active and Provisional Members present.

        (2) The Active and Provisional Members do not, at or within 30 days of the review meeting, vote to reinstate the removed Member to his or her previous status as an Active or Provisional Member or, if such Member was an Active Member, vote to reinstate the Member as a Provisional Member pursuant to Section 2.2(d)

        (3) The absence of a positive vote to reinstate the removed Member within 30 days of the review meeting shall be deemed a vote not to reinstate such Member.

8.  **Transfers of Interests**

    No Member shall have the right to sell, transfer, pledge, encumber or otherwise dispose of all or any part of such Member’s membership interest.

9.  **Other Activities**

    Neither any Active or Provisional Member may engage in any activities which compete with the business of the Company. Each Member may engage in any noncompeting business or other activity of his or her choice independent of the Company without having or incurring any obligation to offer any interest in such noncompeting business or activity to the Company or to any other Member, provided that, the Member takes all reasonable steps to ensure clients of such noncompeting business or other activity understand the business or activity is unrelated to the Company.

10. **Confidentiality**

    Each Member recognizes that he, she or it will receive information about the Company and its operations which is confidential and proprietary to the Company. Each Member agrees to keep such information about the Company received in his or her capacity as a Member confidential and agrees not to disclose that information to any other Person, other than disclosures necessary and appropriate to the conduct of the Company’s business.

Section 3:  Meetings of Active Members
**************************************

1.  **Place of Meetings**

    Meetings shall be held at any place within the State of Ohio stated in any proper notice of meeting.

2.  **Types of Members at Meetings**

    Except for meetings pursuant to Section 2.7(b), Section 8.1(a), or Section 9.2 only Active and Provisional Members may attend meetings. Notwithstanding the foregoing, unless a majority of Active and Provisional Members present at such a meeting object, any Active or Provisional Member attending a meeting may invite Inactive Members, Provisional Employees, or non-Members to attend such a meeting, provided that such invited persons may attend such meeting solely for the purpose of observing it and may not participate in the meeting.

3.  **Regular Meetings**

    Regular meetings of the Active and Provisional Members shall be held not less frequently than every six (6) months, as the Active and Provisional Members may determine from time to time.

4.  **Special Meetings**

    Special meetings of Active and Provisional Members may be called by Active or Provisional Members constituting at least 20% of the votes of all Active and Provisional Members.

5.  **Notice of Meetings**

    (a) No notice need be given of regular meetings held at the regular time and place determined by the Active and Provisional Members, of which written notice has been given to all Active and Provisional Members not less than three days before the first meeting following any change of such time or place.

    (b) Notice of any special meeting shall be given by either: (i) a written notice given not less than three days nor more than 30 days before the date of the meeting to each Active or Provisional Member, or (ii) telephone notice given not less than 48 hours nor more than 10 days before the date of the meeting to each Active or Provisional Member. The notice shall state the place, date and hour of the meeting and the general nature of the business to be transacted. No other business may be transacted at such meeting.

    (c) Notice of an Active and Provisional Members’ meeting shall be given and be deemed to have been given in the manner and at the time specified in Section 9.13.

6.  **Telephonic Meetings**

    Members entitled to participate in a meeting of the Company may do so through the use of conference telephones or similar communications equipment, as long as all Members participating in the meeting can hear one another. Participation in a meeting pursuant to this Section 3.6 constitutes presence in person at that meeting.

7.  **No Proxies**

    All votes and consents on behalf of Active and Provisional Members shall be cast or given by such Member personally and not through any proxy, agent or other representative.

Section 4: Capital Accounts; Distributions of Cash; Investments

1.  **Individual Capital Accounts**

    (a) **Credits to ICAs:** Each Provisional Member’s, Active Member’s and Inactive Member’s ICA shall be credited the initial capital contribution made by such Member pursuant to Section Section 6 and, at the end of each Distribution Period, the ICA Required Payment, (2) the ICA Interest Credit and (3) the ICA Group Investment Credit.

    (b) **Withdrawal from ICAs:** A credit to a Member’s ICA becomes available for withdrawal 24 months after such credit was credited. However, the initial capital contribution credit shall only be available for withdrawal as part of a mandatory payment pursuant to Section 4.1(b)(2) or an automatic payment pursuant to Section 4.1(b)(3). Amounts withdrawn from the ICA will be deducted from the ICA.

        (1) **Withdrawal Upon Request:** To request a withdrawal of ICA credits available for withdrawal, a Member must notify the Company in writing of such request at least 90 days prior to the date the Member wishes to obtain payment. The notice must state the amount to be withdrawn and the desired withdrawal date. If a Member has given proper notice as required in this Section 4.1(b)(1), then such Member shall be paid by the Company on the date requested in the notice.

        (2) **Mandatory Payment:** If an Active or Provisional Member withdraws pursuant to Section 2.5, is removed pursuant to Section 2.7 or becomes an Inactive Member pursuant to Section 2.6, then any balance in such Member’s ICA which was available for distribution on the date such Member became an Inactive Member shall be paid within 90 days.

        (3) **Automatic Payment:** Any credits which become available for withdrawal in an Inactive Member’s ICA shall automatically be paid to such Member within [90] days of becoming available.

        (4) **Payment Limitations:** Any withdrawal or payment under this Section 4.1(b) is limited to the Available Cash of the Company as determined from time to time. If the Available Cash at any time is insufficient to pay all amounts then payable under this Section 4.1(b), then as and to the extent Available Cash is or becomes available, promptly following the end of each month, Amounts payable shall be paid in the following priority groups:

            (i) first, amounts payable pursuant to Section 4.1(b)(3) shall be paid in their entirety;

            (ii) second, any amounts payable pursuant to Section 4.1(b)(2) shall be paid in their entirety; and

            (iii) third, any amounts payable pursuant to Section 4.1(b)(1) shall be paid in their entirety.

        (5) **Tie Breaker:** Within a given priority group in Section 4.1(b)(4) above, amounts that became eligible for payment on an earlier date shall be paid in their entirety prior to those that became eligible on a later date.

.2  *[Intentionally left blank]*

.3  **ICA Investments**

    If the Company has sufficient cash for its operating, expansion and other needs, then it may use ICA funds for investments, provided that any gain or return realized on ICA Group Investments shall be credited to the Members’ ICAs (in accordance with Section 4.1(a)).

.4  **Member Guaranteed Payments**

    The company may, but shall not be obligated, to provide Member Guaranteed Payments to Active Members. If the next distribution that would otherwise be made pursuant to this Section 4 is smaller than the amount of such Member Guaranteed Payment, the excess amount of the advance shall be repaid to the Company on the date such distribution would otherwise be made.

.5  **Distributions from Operations**

    Net Company Profits for each Distribution Period shall be applied as follows:

    (a) First, the credit due on ICAs pursuant to Section 4.1(a)(2) and 4.1(a)(3)for the period shall be deducted from Net Company Profits and credited to the ICAs;

    (b) *[Intentionally left blank]*

    (c) Second, the Member Profit Share Formula shall be applied to the Net Company Profits to calculate the Member Profit Share.

        (1) Then, the Percentage Share is applied to the Member Profit Share to calculate the Member Gross Profit for each Member who rendered services during the Distribution Period

        (2) The ICA Required Payment shall be deducted and an Individual Capital Credit shall be recorded for such Member as of the end of the distribution period in the amount of such deductions;

        (3) The balance shall be distributed to such Member.

6.  **Advances Against Distributions**

    The Company may, but shall not be obligated, to make advances to Members against the next following distribution pursuant to this Section 4. In such case, any such advance shall be repaid by offsetting it against the next distribution that would otherwise be made pursuant to this Section 4. If the next distribution that would otherwise be made pursuant to this Section 4 is smaller than the amount of such advance, the excess amount of the advance shall be repaid to the Company on the date such distribution would otherwise be made.

7. *[Intentionally left blank]*

8.  **Additional Payments to Meet Minimum**

    To the extent (and only to the extent) funds lent to the Company expressly permit the proceeds of such loans to be used for such purposes, the Company may make additional payments to one or more Provisional Members or Active Members in an amount which, together with any distribution payable pursuant to Section 4.5, equals a specified minimum amount offered by the Company to such Provisional Member or Active Member for a specified period. To the extent the amount distributable to such a Member pursuant to Section 4.5 for a Distribution Period within the specified period would otherwise be less than the specified minimum amount offered to such a Member, the ICA Required Payments from such Member shall be reduced or eliminated for such Distribution Period. Such additional payments shall only be available during the initial 24-month period starting upon the admission of the initial Active Members under this Agreement and no additional payments under this Section 4.8 shall be made after the initial 24-month period.

9.  **General**

    No Member shall become entitled to any distribution from the Company except as expressly stated in this Agreement. No Member has any right to demand and receive, or shall be required to accept, any distribution from the Company in any form other than money. No distribution shall be made by the Company if such distribution is prohibited by Chapter 1705 of the Ohio Revised Code (provisions relating to distributions leaving a company unable to pay its debts as they become due in the ordinary course of business or with fewer assets than liabilities). Any Member who receives a distribution from the Company which is determined to have been prohibited by Chapter 1705 of the Ohio Revised Code shall, within 30 days following notice, return such distribution to the Company.

Section 5:  Allocation of Net Income, Etc.
******************************************

    Items of net income, net loss and other tax items will be allocated among the Members as set forth in **Exhibit A**. **“Capital Account”** and certain other terms used in this Agreement are defined as set forth in **Exhibit A**.

Section 6:  Capitalization
**************************

1.  **Initial Contributions**

    A Member initially admitted as a Provisional or Active Member shall make an initial contribution in an amount equal to the value of five hundred dollars ($500). Such initial contribution shall be due in cash within 30 days of such Member being first admitted unless the Company and such Member have agreed that the newly admitted Member shall pay such initial contribution in installments. Installments may be in cash or in agreed value of services rendered to the  Company, but, in either case, the initial contribution credit must be fully paid within one year of a Member’s initial admission.

2.  **Additional Contributions**

    Except for the ICA Required Payments, the Members shall not be required to make additional capital contributions to the Company. The Company may, but shall not be obligated to, issue additional Individual Capital Credits in exchange for additional contributions to the capital of the Company. Such contributions shall earn only the same annual increment as all other ICA balances as described in Section 4.1(a). Any contribution of property other than cash shall be valued for Capital Account purposes at such amount as the contributing Member and the Active and Provisional Members (other than any Member making contribution) may agree.

3.  **Loans**

    Upon the approval of the Active and Provisional Members, the Company may obtain loans from third parties to meet operating expenses and capital expenditures of the Company, which loans may be unsecured or secured by all or a portion of Company assets. If loans from third parties are unavailable on terms and conditions which the Active and Provisional Members deem reasonable, then any Member may, but shall not be obligated to, loan money to the Company. Any loan from a Member shall bear interest at such rate and be payable on such terms as the lending Member and the Active and Provisional Members (other than any Member making the loan) may agree, except that all interest, fees, or other sums to be paid by the Company in consideration for such loan in any given year shall not exceed the amount equivalent to 10% of the outstanding principal amount of such loan for such year.

4.  **Investment of Surplus Capital**

    Upon the approval of the Active and Provisional Members, the Company may invest any surplus capital not needed for the Company’s own operations or expansion in P Squared Controllers Group Investments.  The company may not invest any surplus capital in Outside Investments.

Section 7:  Accounting Matters
******************************

1.  **Maintenance of Records**

    The Company shall maintain at the office referred to in Section 1.5 all of the following:

    (a) A current list of the full name and last known business or residence address of each Member set forth in alphabetical order, together with the contribution and (b) (b) share in profits and losses of each Member.
    (c) A copy of the Articles of Organization and all amendments thereto.
    (c) Copies of the Company’s federal, state and local income tax or information returns and reports, if any, for the six most recent taxable years.
    (d) A copy of this Agreement and any amendments thereto.
    (e) Copies of the financial statements of the Company, if any, for the six most recent Fiscal Years.
    (f) The books and records of the Company as they relate to the internal affairs of the Company for at least the current and past four Fiscal Years.

2.  **Delivery to Members and Inspection**

    Upon the request of a Member, for purposes reasonably related to the interest of that Person as a Member, the Company shall promptly deliver to the Member, at the expense of the Company, a copy of the information required to be maintained by Section 7.1.

3.  **Reports**

    The Company shall send or cause to be sent to each Member within 90 days after the end of each taxable year such information as is necessary to complete federal and state income tax or information returns.

4.  **Tax and Accounting Matters**

    The Company’s taxable year and accounting method for income tax purposes shall be as determined under the Code and Treasury Regulations. The Active Members shall designate from time to time the “tax matters partner” within the meaning of section 6231(a)(7) of the Code. The tax matters partner shall have all of the authority granted by the Code to a tax matters partner, provided that the tax matters partner shall keep each Member informed as to the status of any audit of the Company’s tax affairs.

Section 8:  Dissolution and Liquidation
***************************************

1.  **Events of Dissolution**

    Except as otherwise provided in this Agreement, the Company shall be dissolved and its affairs shall be wound up upon the happening of the first to occur of the following:

    (a) Upon the separate vote or consent to dissolve the company of both:

        (1) Members whose current Percentage Share, in the aggregate, is more than fifty percent (50%) of the current profits of the Company and

        (2) two-thirds of the Active and Provisional Members where each Member shall have such voting rights as are provided for in Section 2.1; or

    (b) Upon the entry of a decree of judicial dissolution pursuant to Chapter 1705 of the Ohio Revised Code.

2.  **Effect of Dissolution**

    Upon any dissolution of the Company under this Agreement or the Act, except as otherwise provided in this Agreement, the continuing operation of the Company’s business shall be confined to those activities reasonably necessary to wind up the Company’s affairs, discharge its obligations, and liquidate its assets and properties in a businesslike manner. Upon the occurrence of an event of dissolution, unless the business of the Company is continued as provided herein, the Company shall file a certificate of cancellation under Chapter 1705 of the Ohio Revised Code.

3.  **Liquidation and Termination**

    (a) If the Company is dissolved, then an accounting of the Company’s assets, liabilities and operations through the last day of the month in which the dissolution occurs shall be made, and the affairs of the Company shall thereafter be promptly wound up and terminated. The Active and Provisional Members shall designate a liquidating trustee of the Company. The liquidating trustee will be responsible for winding up and terminating the affairs of the Company and will determine all matters in connection therewith (including, without limitation, the arrangements to be made with creditors, to what extent and under what terms the assets of the Company are to be sold, and the amount or necessity of cash reserves to cover contingent liabilities) as the liquidating trustee deems advisable and proper; provided, however, that all decisions of the liquidating trustee will be made in accordance with the fiduciary duty owed by the liquidating trustee to the Company and each of the Members. The liquidating trustee will liquidate the assets of the Company as promptly as is consistent with obtaining the fair market value thereof, subject to the limitation that the capital stock of the Permanent Capital Member owned by the Company shall be sold only to the extent required to provide for the payment and discharge referred to in clause (1) below. The proceeds therefrom, to the extent sufficient therefor, will be applied and distributed in the following order:

        (1) To the payment and discharge of all of the Company’s debts and liabilities to creditors (including Members) in the order of priority as provided by law, other than liabilities for distributions to Members; and

        (2) To the repayment of unpaid Individual Capital Credits, in the priorities set forth in Sections 4.1(b)(4) and 4.1(b)(5).

    (b) After all of the assets of the Company have been distributed, the Company shall terminate; however, if at any time thereafter any funds in any cash reserve fund referred to in Section 8.3(a) are released because the need for the cash reserve fund has ended, the funds shall be distributed in accordance with the priorities laid out in Section 8.3(a).

    (c) Notwithstanding anything to the contrary in this Agreement, upon a liquidation within the meaning of Treasury Regulation Section 1.704-1(b)(2)(ii)(g), if any Member has a deficit or negative balance in the Member’s ICA (after giving effect to all contributions, distributions, allocations, and other Capital Account adjustments for all taxable years, including the year during which such liquidation occurs), the Member shall have no obligation to make any capital contribution to the Company, and the negative balance of the Member’s ICA shall not be considered a debt owed by the Member to the Company or to any other Person for any purpose whatsoever.

4.  **Certificate of Cancellation**

    Upon the completion of the winding up of the affairs of the Company, the Company shall file with the Ohio Secretary of State a certificate of cancellation in accordance with Chapter 1705 of the Ohio Revised Code.

5.  **Recourse to Assets**

    Each Member shall look solely to the assets of the Company for any profits or return of his, her or its capital contributions. If the assets remaining after the payment or discharge of the debts and liabilities of the Company are insufficient to return a Member’s capital contributions or profits, the Member shall have no recourse against the Company or any other Member.

Section 9:  General Provisions
******************************

1.  **Indemnification**

    The Company shall indemnify and hold harmless, to the fullest extent permitted by law each Member (each of the foregoing, an **“Indemnified Party”**), as follows:

    (a) The Company shall indemnify and hold harmless, to the fullest extent permitted by law, any Indemnified Party from and against any and all losses, claims, damages, liabilities, expenses (including legal fees and expenses), judgments, fines, settlements and other amounts (**“Indemnified Costs”**) arising from all claims, demands, actions, suits or proceedings (**“Actions”**), whether civil, criminal, administrative or investigative, in which the Indemnified Party may be involved, or threatened to be involved, as a party or otherwise arising as a result of its status as a Member of the Company regardless of whether the Indemnified Party continues in the capacity at the time the liability or expense is paid or incurred, and regardless of whether the Action is brought by a third party, a Member, or by or in the right of the Company; provided, however, no such Person shall be indemnified for any Indemnified Costs which proximately result from the Person’s fraud, bad faith, gross negligence, willful misconduct or material breach of this Agreement.

    (b) The Company shall pay or reimburse, to the fullest extent allowed by law and consistent with Section 9.1(a), in advance of the final disposition of the proceeding, Indemnified Costs incurred by the Indemnified Party in connection with any Action that is the subject of Section 9.1(a), unless the request for indemnification pertains to an Action brought against the Indemnified Party by the Company (including any counterclaim, cross-complaint or the like brought by the Company).

    (c) Notwithstanding any other provision of this Section 9.1, the Company shall pay or reimburse Indemnified Costs incurred by an Indemnified Party in connection with such Person’s appearance as a witness or other participation in a proceeding involving or affecting the Company at a time when the Indemnified Party is not a named defendant or respondent in the proceeding.

    (d) The Company may, but shall not be obligated to, purchase and maintain insurance or other arrangements on behalf of the Indemnified Parties against any liability asserted against any Indemnified Party and incurred by any Indemnified Party in that capacity, or arising out of the Indemnified Party’s status in that capacity, regardless of whether the Company would have the power to indemnify the Indemnified Party against that liability under this Section 9.1. The indemnification provided by this Section 9.1 shall be in addition to any other rights to which the Indemnified Parties may be entitled under any agreement, any vote of the Active and Provisional Members (other than any Member seeking indemnification), as a matter of law or otherwise, and shall inure to the benefit of the heirs, successors, assigns and administrators of the Indemnified Party.

    (e) An Indemnified Party shall not be denied indemnification in whole or in part under this Section 9.1 because the Indemnified Party had an interest in the transaction with respect to which the indemnification applies if the transaction was otherwise permitted by the terms of this Agreement.

2.  **Amendments**

    This Agreement may be amended in whole or in part only upon the separate vote or consent of (1) Members whose current Percentage Share, in the aggregate, is more than fifty percent (50%) of the current profits of the Company and (2) two-thirds of the Active and Provisional Members, with each having such voting rights as are provided under Section 2.1. Any amendment receiving such approval shall be effective without additional signatures of the Members other than the Members who have approved the amendment, and shall be in writing, dated and signed by the Active and Provisional Members voting therefor or consenting thereto. If any conflict arises between the provisions of the amendment, or amendments, and the terms hereof, the most recent provisions shall govern and control. The Articles of Organization shall be amended whenever required by the provisions of this Agreement or by the Act. Any such amended Articles shall be filed for record by the Company as required by the Act, and this Agreement shall also be amended as necessary to reflect such change.

3.  *[Intentionally left blank]*

4.  **Mergers**

    Any decision by the Company to merge shall first be approved by the separate vote or consent of (1) Members whose current Percentage Share, in the aggregate, is more than fifty percent (50%) of the current profits of the Company and (2) two-thirds of the Active and Provisional Members, with each having such voting rights as are provided under Section 2.1.

5.  **Governing Law**

    This Agreement and the rights of the parties hereunder will be governed by, interpreted, and enforced in accordance with the laws of the state of Ohio without regard to the principles governing conflicts of laws.

6.  **Arbitration**

    Any dispute, controversy, or claim arising out of or in connection with, or relating to, this Agreement or any breach or alleged breach hereof shall, upon the request of any party involved, be submitted to, and settled by, arbitration in the county in which the main office of the Company is located and pursuant to the commercial arbitration rules then in effect of the American Arbitration Association (or at any time or at any other place or under any other form of arbitration mutually acceptable to the parties so involved). Any award rendered shall be final and conclusive upon the parties and a judgment thereon may be entered in the highest court of the forum, state or federal, having jurisdiction. The expenses of the arbitration shall be borne equally by the parties to the arbitration, provided that each party shall pay for and bear the cost of its own experts, evidence and counsel’s fees, except that in the discretion of the arbitrator, any award may include the cost of a party’s counsel if the arbitrator expressly determines that the party against whom such award is entered has caused the dispute, controversy or claim to be submitted to arbitration as a dilatory tactic.

7.  **Binding Effect**

    This Agreement will be binding upon and inure to the benefit of the Members, and their respective distributees, successors and assigns; provided, however, nothing contained in this Section 9.7 shall limit the effectiveness of any restriction on transfers of membership interests.

8.  **Headings**

    All headings are inserted only for convenience and ease of reference and are not to be considered in the construction or interpretation of any provision of this Agreement.

9.  **Severability**

    If any provision of this Agreement is held to be illegal, invalid or unenforceable under the present or future laws effective during the term of this Agreement, the provision will be fully severable; this Agreement will be construed and enforced as if the illegal, invalid, or unenforceable provision had never comprised a part of this Agreement; and the remaining provisions of this Agreement will remain in full force and effect and will not be affected by the illegal, invalid or unenforceable provision or by its severance from this Agreement. Furthermore, in lieu of the illegal, invalid or unenforceable provision, there will be added automatically as a part of this Agreement a provision as similar in terms to the illegal, invalid or unenforceable provision as may be possible and be legal, valid and enforceable.

10. **Multiple Counterparts**

    This Agreement may be executed in multiple counterparts, each of which shall be deemed an original, and counterpart signature pages may be assembled to form a single original document.

11. **Additional Documents and Acts**

    Each Member agrees to execute and deliver such additional documents and instruments and to perform such additional acts as may be necessary or appropriate to effectuate, carry out and perform all of the terms, provisions and conditions of this Agreement and the transactions contemplated by this Agreement.

12. **No Third Party Beneficiary**

    This Agreement is made solely and specifically among and for the benefit of the parties, and their respective successors and assigns subject to the express provisions relating to successors and assigns, and no other Person will have any rights, interest or claims or be entitled to any benefits under or on account of this Agreement as a third party beneficiary or otherwise. None of the provisions of this Agreement shall be for the benefit of or enforceable by any creditors of the Company.

13. **Notices**

    All notices, consents, requests, demands or other communications to or upon the respective parties shall be in writing and shall be effective for all purposes upon receipt on any business day before 5:00 PM local time and on the next business day if received after 5:00 PM or on other than a business day, including without limitation, in the case of (i) personal delivery, (ii) delivery by messenger, express or air courier or similar courier, (iii) delivery by United States first class certified or registered mail, postage prepaid. In this Section 9.13 “business days” means days other than Saturdays, Sundays and federal and Ohio legal holidays. Either party may change its address by written notice to the other in the manner set forth above. Receipt of communications by United States first class or registered mail will be sufficiently evidenced by return receipt.

14. **Title to Company Property**

    Legal title to all property of the Company will be held and conveyed in the name of the Company.

15. **Waiver**

    No waiver of any obligations under this Agreement will be enforceable or admissible unless set forth in a writing signed by the party against which enforcement or admission is sought. No delay or failure to require performance of any provision of this Agreement shall constitute a waiver of that provision as to that or any other instance. Any waiver granted shall apply solely to the specific instance expressly stated.

16. **Entire Agreement**

    This Agreement and the Exhibits contain the entire understanding between the Members and supersede any prior written or oral agreements between them regarding the same subject matter. There are no representations, agreements, arrangements or understandings, oral or written, between the Members relating to the subject matter of this Agreement which are not fully expressed in this Agreement.

17. **No State Law Partnership**

    The Members intend that the Company not be a partnership (including, without limitation, a limited partnership) or joint venture, and that no Member be an agent, partner or joint venturer of any other Member, for any purposes other than federal and state tax purposes, and this Agreement shall not be construed to suggest otherwise.

18. **Determination of Matters Not Provided for in This Agreement**

    The Active and Provisional Members shall decide any questions arising with respect to the Company and this Agreement which are not specifically or expressly provided for in this Agreement.

19. **Further Assurances**

    Each Member agrees to cooperate and to execute and deliver in a timely fashion any and all additional documents necessary to effectuate the purposes of the Company and this Agreement.

20. *[Intentionally left blank]*

21. **Certificate of Interest**

    The Company may issue one or more certificate confirming an Active Member’s membership interest, but neither any purported transfer nor endorsement of any such certificate shall have any effect on the ownership of the membership interest confirmed thereby.

Section 10:  Definitions
************************************

    For purposes of this Agreement, the following terms have the following meanings:

    **“Act”**
        means Chapter 1705 of the Ohio Revised Code, as amended from time to time.

    **“Active Member”**
        is defined in Section 2.1(b).

    **“Available Cash”**
        means cash on hand in excess of the reasonable working capital needs of the Company, as determined from time to time by the Active and Provisional Members. For purposes of this Agreement, **“Cash”** includes both cash and cash equivalents.

    **“Capital Account”**
        is defined in Exhibit A.

    **“Code”**
        means the Internal Revenue Code of 1986 or any successor statute, as amended from time to time.

    **“Distribution Period”**
        means the regular period determined by the Active and Provisional Members for the calculation and payment of distributions pursuant to Section 4.1.

    **“Fiscal Year”**
        means the calendar year, ending on each December 31.

    **“ICA”** or **“Individual Capital Account”**
        means an account maintained on the books of the Company for each Provisional Member, Active Member and Inactive Member.

    **“ICA Group Investment Credit”**
        means an Active or Provisional Member’s Percentage Share of interest, dividends or other returns received from P Squared Controllers Group Investments.

    **“ICA Interest Credit”**
        means, at the end of each Distribution Period, an increment in the amount equivalent to an annual rate of return, adopted by the Active and Provisional Members from time to time, not to exceed eight percent (8%), on the amount in the Member’s ICA at the beginning of such Distribution Period.

    **“ICA Required Payment”**
        means a uniform percentage of each Active Member’s Member Gross Profit, as determined from time to time by the Active Members, but the Individual Capital Required Contribution shall not be less than 5% of the Member Gross Profit.

    **“Inactive Member”**
        is defined in Section 2.1(c).

    **“Individual Capital Credit”**
        means a credit recorded for a Member in accordance with Section 4.5(c)(2).

    **“Member Guaranteed Payments”**
        are early distributions to Active Members calculated based on the Percentage Share.

    **“Member Profit Share Formula”**
        is the percentage of Net Company Profits used to calculate the Member Profit Share, and is determined from time to time by the Active Members.

    **“Member Profit Share”**
        is the percentage of Net Company Profits that is allocated to Active Members during a Distribution Period pursuant to 4.5.

    **“Member Gross Profit”**
        means for each Member, such Member’s Percentage Share of Net Company Profits received during a Distribution Period pursuant to Section 4.5.

    **“Multi-Period Expense”**
        means a cash expenditure by the Company which the Active and Provisional Members determine should be allocated to more than one Distribution Period. Such allocation period shall not be longer than three (3) Distribution Periods and no more than seven percent (7%) of cash expenditures from one Distribution Period may be allocated to one or more future Distribution Periods.

    **“Net Company Profits”**
        means cash received during a Distribution Period from operations less the sum of (i) cash outlays during such period including Member Guaranteed Payments made pursuant to 4.4, other than Multi-Period Expenses and distributions pursuant to Section 4.5 but including any additional payments under Section 4.8, and (ii) the portion of Multi-Period Expenses which are allocable to such Distribution Period based on the multi-period allocations determined by the Active and Provisional Members for such outlays. Additional payments under Section 4.8 shall be attributed to the Distribution Period in which such additional payments are made and not to the Distribution Period whose Net Company Profits were used in determining the amount of the additional payments.

    **“Net Income”**
        is defined in Exhibit A.

    **“Net Loss”**
        is defined in Exhibit A.

    **“Outside Investment”**
        means any investment that does not qualify as a P Squared Controllers Group Investment.

    **“Person”**
        means an individual or a legal entity.

    **“Percentage Share”**
        means a percentage calculated for each Member for each Distribution Period as determined by a formula adopted by the Active and Provisional Members from time to time. Such formula shall reflect relative amounts and value of different services contributed to the Company and shall not be based on the balances of Members’ ICAs. Such formula shall be constructed such that the sum of all Members’ Percentage Shares for a given Distribution Period equals 100%.

    **“Provisional Employee”**
        means an employee of the Company serving a trial period for admission as an Active Member and who has no voting rights or an economic interest in the Company.

    **“Provisional Member”**
        is defined in Section 2.1(a).

    **“Protected Member”**
        is defined in Section 2.1(e).

    **“P Squared Controllers Group Investment”**
        means any investment, loan, guaranty, or other financial assistance provided by the Company to an entity which, at the time such assistance is provided, is a licensee of P Squared Controllers LLC.

    **“Treasury Regulations”**
        means final, temporary and proposed income tax regulations issued by the U. S. Treasury Department.

