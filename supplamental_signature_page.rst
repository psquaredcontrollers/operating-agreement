Supplamental Signature Page
###########################

This Supplemental Signature Page to the Operating Agreement dated as of January 6, 2017, of P Squared Controllers LLC, an Ohio limited liability company (the “Agreement”), is executed, delivered and accepted as of the date set forth below.

:Date: 2017-01-06
:Revision: Initial Revision

Provisional Members:
--------------------

| The undersigned is hereby designated as a Provisional Member under, and agrees to be bound by, the terms of the Agreement.  In accordance with Section 2.2(b)(ii) of the Agreement, the undersigned acknowledges that Members are not employees and, therefore, not entitled to benefits employees may be entitled to, including, without limitation, workers’ compensation.
|
| Signature: ______________________ Date: __________
| Printed Name: __________________________________

Accepted: P Squared Controllers, LLC
------------------------------------

| Signature: ______________________ Date: __________
| Printed Name: __________________________________
| Title: ________________________________________
|
| Signature: ______________________ Date: __________
| Printed Name: __________________________________
| Title: ________________________________________
